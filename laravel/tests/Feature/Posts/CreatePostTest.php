<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use Webmozart\Assert\Assert;

class CreatePostTest extends TestCase
{
    /** @test */
    public function user_can_create_if_data_is_valid()
    {
        $dataCreate = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('name', $dataCreate['name'])
                ->where('email', $dataCreate['email'])
                ->etc()
            )->etc()
        );

        $this->assertDatabaseHas('posts', [
            'name' => $dataCreate['name'],
            'email' => $dataCreate['email']
        ]);
    }

    /** @test */
    public function user_can_not_create_if_name_is_null()
    {
        $dataCreate = [
            'name' => '',
            'email' => $this->faker->email,
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('name')
                ->etc()
            )->etc()
        );
    }

    /** @test */
    public function user_can_not_create_if_email_is_null()
    {
        $dataCreate = [
            'name' => $this->faker->name,
            'email' => '',
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('errors', fn (AssertableJson $json) =>
                $json->has('email')
                ->etc()
            )->etc()
        );
    }

    /** @test */
    public function user_can_not_create_if_data_is_not_valid()
    {
        $dataCreate = [
            'name' => '',
            'email' => '',
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->has('errors', fn (AssertableJson $json) =>
            $json->has('email')
                ->has('name')
                ->etc()
            )->etc()
        );
    }
}
